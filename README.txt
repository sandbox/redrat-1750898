-- FOREWARNING --

As you can see, my English is really ugly and awkward, so don't hesitate
to point out all my obvious mistakes or even send me patches for module
messages and documentation. Thank you in advance!

-- SUMMARY --

The Date Range module implements a custom field which allows you to enter
dates and periods as a text string with an arbitrary precision, e.g.:

'1991' - a period, consisted of all days of year 1991;
'2012.02' - all days of February 2012;
'1917.10.25' - October 25, 1917 exactly;
'1941-1945' - a period from the beginning of 1941 till the end of 1945;
'1234.05.06-7890' - a period from May 06, 1234 till the end of year 7890.

A general pattern for field input is: YYYY[.MM[.DD]][-YYYY[.MM[.DD]]].

The module also implements a custom filter for Views which allows you to
select nodes which are before, after, inclide, exclude or exactly equal
to the specified period.

-- REQUIREMENTS --

Mandatory: CCK for 6.x branch.

Optional: Views 2.x or 3.x for 6.x and 7.x branches.

-- CONFIGURATION --

You can select would this field will represent a single date in form of
YYYY[.MM[.DD]] or a period as described above.

<?php

/**
 * @file
 * Common functions for all branches of the Date Range module.
 */

/**
 * Checks field's value for emptiness.
 */
function _date_range_empty($var) {
  return empty($var['date1']) || empty($var['date2']);
}

/**
 * Finds a last day of the specified month.
 */
function _date_range_last_day($year, $month) {
  for ($day = 31; $day >= 28; $day--) {
    if (checkdate($month, $day, $year)) {
      return (string) $day;
    }
  }
}

/**
 * Converts an input string to the array of dates.
 *
 * @param string $str
 *   A string containing an input value for a date range.
 *
 * @param bool $period
 *   (optional)Should this string be treated as a period or a single date?
 *
 * @param string|'.' $delim
 *   (optional)A delimeter character used for splitting fields in the input
 *   string.
 *
 * @return array|false
 *   An array of separate fields of date range or a FALSE if the input string
 *   has invalid format.
 */
function _date_range_str2var($str, $period = TRUE, $delim = '.') {
  if ($period) {
    $pattern = '#^(?P<year1>[1-9]\d{3})(?:(?:\.(?P<month1>0[1-9]|1[0-2]))(?:\.(?P<day1>0[1-9]|[12]\d|3[01]))?)?(?:(?:-(?P<year2>[1-9]\d{3}))(?:(?:\.(?P<month2>0[1-9]|1[0-2]))(?:\.(?P<day2>0[1-9]|[12]\d|3[01]))?)?)?$#';
  }
  else {
    $pattern = '#^(?P<year1>[1-9]\d{3})(?:(?:\.(?P<month1>0[1-9]|1[0-2]))(?:\.(?P<day1>0[1-9]|[12]\d|3[01]))?)?$#';
  }

  if (!preg_match($pattern, $str, $var)) {
    return FALSE;
  }

  if (empty($var['year2'])) {
    $var['year2'] = $var['year1'];
    $var['month2'] = (empty($var['month1'])) ? '12' : $var['month1'];
    $var['day2'] = (empty($var['day1'])) ? _date_range_last_day($var['year2'], $var['month2']) : $var['day1'];
  }
  else {
    if (empty($var['month2'])) {
      $var['month2'] = '12';
    }
    if (empty($var['day2'])) {
      $var['day2'] = _date_range_last_day($var['year2'], $var['month2']);
    }
  }

  if (empty($var['month1'])) {
    $var['month1'] = '01';
  }

  if (empty($var['day1'])) {
    $var['day1'] = '01';
  }

  return $var;
}

/**
 * Converts two dates of a date range to the string.
 *
 * @param string $date1
 *   A string containing the first date of range.
 *
 * @param string $date2
 *   A string containing the last date of range.
 *
 * @param bool $expanded
 *   (optional)Should resulting string has a compact or an expanded format?
 *
 * @param string|'.' $delim
 *   (optional)A delimeter character used for concatenating fields in the
 *   resulting string.
 *
 * @return string
 *   Formatted string.
 */
function _date_range_var2str($date1, $date2, $expanded = FALSE, $delim = '.') {
  $d1 = array(substr($date1, 0, 4), substr($date1, 4, 2), substr($date1, 6, 2));
  $d2 = array(substr($date2, 0, 4), substr($date2, 4, 2), substr($date2, 6, 2));

  if ($d1 == $d2) {
    return implode($delim, $d1);
  }

  if ($expanded) {
    return implode($delim, $d1) . '-' . implode($delim, $d2);
  }

  if ($d1[2] == '01') {
    unset($d1[2]);
  }

  if ($d2[2] == _date_range_last_day($d2[0], $d2[1])) {
    unset($d2[2]);
  }

  if ($d1 == $d2) {
    return implode($delim, $d1);
  }

  if (count($d1) == 2 && $d1[1] == '01') {
    unset($d1[1]);
  }

  if (count($d2) == 2 && $d2[1] == '12') {
    unset($d2[1]);
  }

  return ($d1 == $d2) ? implode($delim, $d1) : implode($delim, $d1) . '-' . implode($delim, $d2);
}

/**
 * Checks input string for correctness.
 *
 * @param string $str
 *   A string containing an input value for a date range.
 *
 * @param bool $period
 *   (optional)Should this string be treated as a period or a single date?
 *
 * @return array|string
 *   Array of separate dates of a date range or a string with error message
 *   if an input value has invalid format.
 */
function _date_range_validate($str, $period = TRUE) {
  if (!($var = _date_range_str2var($str, $period))) {
    return t('Invalid format of date range.');
  }
  elseif (!checkdate($var['month1'], $var['day1'], $var['year1'])) {
    return t('Invalid day of month for first date.');
  }
  elseif (!checkdate($var['month2'], $var['day2'], $var['year2'])) {
    return t('Invalid day of month for last date.');
  }
  elseif ($var['year1'] . $var['month1'] . $var['day1'] > $var['year2'] . $var['month2'] . $var['day2']) {
    return t('First date must be lesser then second date.');
  }
  else {
    return array(
      'date1' => $var['year1'] . $var['month1'] . $var['day1'],
      'date2' => $var['year2'] . $var['month2'] . $var['day2'],
    );
  }
}

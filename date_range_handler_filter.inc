<?php

/**
 * @file
 * Definition of date_range_handler_filter.
 */

class date_range_handler_filter extends views_handler_filter {

  var $always_multiple = TRUE;

  function option_definition() {
    $options = parent::option_definition();
    $options['operator']['default'] = 'include';
    return $options;
  }

  function operators() {
    $operators = array(
      'exactly' => array(
        'title' => t('Is exactly'),
        'values' => TRUE,
      ),
      'before' => array(
        'title' => t('Is before'),
        'values' => TRUE,
      ),
      'starts' => array(
        'title' => t('Starts from'),
        'values' => TRUE,
      ),
      'include' => array(
        'title' => t('Includes'),
        'values' => TRUE,
      ),
      'exclude' => array(
        'title' => t('Does not include'),
        'values' => TRUE,
      ),
    );
    if (!empty($this->definition['allow empty'])) {
      $operators += array(
        'empty' => array(
          'title' => t('Is empty'),
          'values' => FALSE,
        ),
        'filled' => array(
          'title' => t('Is filled'),
          'values' => FALSE,
        ),
      );
    }
    return $operators;
  }

  function operator_options() {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info['title'];
    }
    return $options;
  }

  function operator_values() {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if ($info['values'] == TRUE) {
        $options[] = $id;
      }
    }
    return $options;
  }

  function admin_summary() {
    if ($this->is_a_group()) {
      return t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $options = $this->operator_options();
    $output = check_plain($options[$this->operator]);
    // $output = check_plain(t($this->operator));
    if (in_array($this->operator, $this->operator_values())) {
      $output .= ' ' . check_plain($this->value);
    }
    return $output;
  }

  function value_form(&$form, &$form_state) {
    $which = 'all';
    if (!empty($form['operator'])) {
      $source = ($form['operator']['#type'] == 'radios') ? 'radio:options[operator]' : 'edit-options-operator';
    }

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator_id'])) {
        $which = in_array($this->operator, $this->operator_values()) ? 'value' : 'none';
      }
      else {
        $source = 'edit-' . drupal_html_id($this->options['expose']['operator_id']);
      }
    }

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      // '#title' => empty($form_state['exposed']) ? t('Value') : '',
      '#size' => 25,
      '#default_value' => $this->value,
    );

    if ($which == 'all') {
      $form['value'] += array(
        '#dependency' => array($source => $this->operator_values()),
      );
    }

    if ($which == 'none') {
      $form['value'] += array(
        '#disabled' => TRUE,
      );
      $form['value']['#default_value'] = t($this->operator);
    }
  }

  function value_validate($form, &$form_state) {
    $value = $form_state['values']['options']['value'];
    $oper = $form_state['values']['options']['operator'];
    if (in_array($oper, $this->operator_values()) && (!empty($value) || $value == '0')) {
      module_load_include('inc', 'date_range', 'date_range');
      $var = _date_range_validate($value);
      if (is_string($var)) {
        form_set_error('options][value', $var);
      }
    }
  }

  function exposed_validate(&$form, &$form_state) {
    if (empty($this->options['exposed'])) {
      return;
    }

    $identifier = $this->options['expose']['identifier'];

    if (empty($identifier)) {
      return;
    }

    if (!empty($this->options['expose']['use_operator']) && !empty($this->options['expose']['operator_id'])) {
      $oper = $form_state['values'][$this->options['expose']['operator_id']];
    }
    else {
      $oper = $this->operator;
    }

    $value = $form_state['values'][$identifier];

    if (in_array($oper, $this->operator_values())) {
      if (!empty($value) || $value == '0') {
        module_load_include('inc', 'date_range', 'date_range');
        $var = _date_range_validate($value);
        if (is_string($var)) {
          form_set_error($identifier, $var);
        }
      }
      else {
        if (!empty($this->options['expose']['required'])) {
          form_set_error($identifier, t('This field is required and must be set.'));
        }
      }
    }
  }

  function query() {
    $this->ensure_my_table();
    $field1 = "$this->table_alias.$this->real_field";
    $field2 = substr_replace($field1, '2', -1);
    $options = $this->operators();

    if ($options[$this->operator]['values']) {
      module_load_include('inc', 'date_range', 'date_range');
      $var = _date_range_str2var($this->value);
      $date1 = $var['year1'] . $var['month1'] . $var['day1'];
      $date2 = $var['year2'] . $var['month2'] . $var['day2'];
    }

    switch ($this->operator) {
      case 'exactly':
        $cond = db_and()
            -> condition($field1, $date1, '=')
            -> condition($field2, $date2, '=');
        break;

      case 'before':
        $cond = db_and()
            -> condition($field1, $date1, '<');
        break;

      case 'starts':
        $cond = db_and()
            -> condition($field2, $date1, '>=');
        break;

      case 'include':
        $cond = db_and()
            -> condition($field1, $date2, '<=')
            -> condition($field2, $date1, '>=');
        break;

      case 'exclude':
        $cond = db_or()
            -> condition($field1, $date2, '>')
            -> condition($field2, $date1, '<');
        break;

      case 'empty':
        $cond = db_or()
            -> condition($field1, NULL, 'IS NULL')
            -> condition($field2, NULL, 'IS NULL');
        break;

      case 'filled':
        $cond = db_and()
            -> condition($field1, NULL, 'IS NOT NULL')
            -> condition($field2, NULL, 'IS NOT NULL');
        break;
    }
    $this->query->add_where($this->options['group'], $cond);
  }
}
